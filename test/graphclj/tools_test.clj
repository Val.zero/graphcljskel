(ns graphclj.tools-test
  (:require [clojure.test :refer :all]
            [graphclj.tools :refer :all :as tools]
            [graphclj.centrality :refer :all :as centr]))

(deftest min-rank-test
  (testing "Le mininmum est-il correct ?"
    (let [g (centr/closeness-all {1 {:neigh #{0 4 3}}
                                  0 {:neigh #{1 3}}
                                  3 {:neigh #{0 1 2}}
                                  4 {:neigh #{1}}
                                  2 {:neigh #{3}}})]
      (is (or (= (tools/min-rank g :close) 4) (= (tools/min-rank g :close) 2)))
      (is (= (tools/min-rank {} :close) nil)))))

(deftest rank-nodes-test
  (testing "Le classement est-il correct ?"
    (let [g (centr/closeness-all {1 {:neigh #{0 4 3}}
                                  0 {:neigh #{1 3}}
                                  3 {:neigh #{0 1 2}}
                                  4 {:neigh #{1}}
                                  2 {:neigh #{3}}})
          g2 (tools/rank-nodes g :close)]
      (is (= (get (get g2 0) :rank) 2))
      (is (= (get (get g2 1) :rank) 3))
      (is (= (get (get g2 2) :rank) 0))
      (is (= (get (get g2 3) :rank) 3))
      (is (= (get (get g2 4) :rank) 0)))))

(deftest max-rank-test
  (testing "Le rang max est-il correct ?"
    (let [g (tools/rank-nodes (centr/closeness-all {1 {:neigh #{0 4 3}}
                                                    0 {:neigh #{1 3}}
                                                    3 {:neigh #{0 1 2}}
                                                    4 {:neigh #{1}}
                                                    2 {:neigh #{3}}}) :close)]
      (is (= (tools/max-rank g) 3)))))

(deftest to-dot-test
  (testing "La conversion est-elle correcte ?"
    (let [g (tools/rank-nodes (centr/closeness-all {1 {:neigh #{0 4 3}}
                                                    0 {:neigh #{1 3}}
                                                    3 {:neigh #{0 1 2}}
                                                    4 {:neigh #{1}}
                                                    2 {:neigh #{3}}}) :close)]
      ;;(print (tools/to-dot g))
      (is (= 1 1)))))
