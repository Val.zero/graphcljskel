(ns graphclj.centrality-test
  (:require [clojure.test :refer :all]
            [graphclj.centrality :refer :all :as centr]))

(deftest degree-test
  (testing "Les degrés sont-ils corrects ?"
    (let [g (centr/degrees {1 {:neigh #{0}}, 0 {:neigh #{1 2}}, 3 {:neigh #{2}}, 2 {:neigh #{0 3}}})]
      (is (= (get (get g 0) :degree) 2))
      (is (= (get (get g 1) :degree) 1))
      (is (= (get (get g 2) :degree) 2))
      (is (= (get (get g 3) :degree) 1)))))

(deftest min-map-test
  (testing "Le mininmum est-il correct ?"
    (is (= (centr/min-map {:a 1, :b 2, :c -5, :d 42}) :c))
    (is (= (centr/min-map {}) nil))))

(deftest new-open-test
  (testing "La liste des sommets ouverts est-elle correcte ?"
    (let [prev-open {2 1.0, 3 1.0, 4 3.0},
          node 2,
          dist 1.0,
          neigh #{1 3 4 5},
          cur-res {1 0.0}]
      (is (= (centr/new-open prev-open node dist neigh cur-res) {3 1.0, 4 2.0, 5 2.0})))))

(deftest distance-test
  (testing "Les distances sont-elles correctes ?"
    (let [g {1 {:neigh #{0 4 3}},
             0 {:neigh #{1 3}},
             3 {:neigh #{0 1 2}},
             4 {:neigh #{1}},
             2 {:neigh #{3}}}]
      (is (= (centr/distance g 1) {0 1.0, 4 1.0, 3 1.0, 1 0.0, 2 2.0})))))

(deftest inverse-or-zero-test
  (testing "L'inverse est-il correct ?"
    (is (= (centr/inverse-or-zero 4) 0.25))
    (is (= (centr/inverse-or-zero 1) 1.0))
    (is (= (centr/inverse-or-zero 0) 0.0))))

(deftest closeness-test
  (testing "La proximité est-celle correcte ?"
    (let [g {1 {:neigh #{0 4 3}}
             0 {:neigh #{1 3}}
             3 {:neigh #{0 1 2}}
             4 {:neigh #{1}}
             2 {:neigh #{3}}}]
      (is (= (centr/closeness g 1) 3.5)))))

(deftest closeness-all-test
  (testing "Les proximités sont-elles correctes ?"
    (let [g {1 {:neigh #{0 4 3}}
             0 {:neigh #{1 3}}
             3 {:neigh #{0 1 2}}
             4 {:neigh #{1}}
             2 {:neigh #{3}}}
          g2 (centr/closeness-all g)]
      (is (= (get (get g2 0) :close) 3.0))
      (is (= (get (get g2 1) :close) 3.5))
      (is (= (get (get g2 2) :close) 2.333333343267441))
      (is (= (get (get g2 3) :close) 3.5))
      (is (= (get (get g2 4) :close) 2.333333343267441)))))
