(ns graphclj.graph-test
  (:require [clojure.test :refer :all]
            [graphclj.graph :refer :all :as graph]))

(deftest gen-graph-test
  (testing "Génération d'un graphe à partir d'un vecteur de paires."
    (is (= 
         (graph/gen-graph ["0 1" "2 3" "0 2"])
         {1 {:neigh #{0}}, 0 {:neigh #{1 2}}, 3 {:neigh #{2}}, 2 {:neigh #{0 3}}}))))

(deftest erdos-renyi-rnd-test
  (testing "La génération semble-t-elle aléatoire ?"
    (is (not= 
         (repeat 30 (graph/erdos-renyi-rnd 3 0.5))
         (repeat 30 (graph/erdos-renyi-rnd 3 0.5)))))
  (testing "Le graph contient-il un nombre correct d'éléments ?"
    (is (<= (count (graph/erdos-renyi-rnd 3 0.5)) 3))
    (is (= (count (graph/erdos-renyi-rnd 3 1)) 3))
    (is (= (count (graph/erdos-renyi-rnd 3 0)) 0)))
  (testing "Les voisins sont-ils corrects ?"
    (is (= (get (graph/erdos-renyi-rnd 3 1) 1) {:neigh #{0 2}}))))