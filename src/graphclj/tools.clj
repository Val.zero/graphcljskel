(ns graphclj.tools
  (:require [clojure.string :as str]
            [graphclj.centrality :as centr]))
(use 'clojure.java.io)

(defn readfile
  "Returns a sequence from a file f"
  [f]
  (with-open [rdr (reader f)]
    (doall (line-seq rdr))))

(defn writefile
  "Writes the string str in a file f"
  [f str]
  (with-open [wrtr (writer f)]
    (.write wrtr str)))

(defn min-rank
  "Returns the node (key) corresponding to the graph's minimum value of label l"
  [g l]
  (if (empty? g)
    nil
    (loop [g g, minkey (ffirst g), minval (get (get g minkey) l)]
      (if (seq g)
        (if (< (get (second (first g)) l) minval)
          (recur (rest g) (ffirst g) (get (second (first g)) l))
          (recur (rest g) minkey minval))
        minkey))))

(defn rank-nodes
  "Ranks the nodes of the graph in relation to label l in accending order"
  [g,l]
  (loop [g2 g, list []]
    (if (seq g2)
      (let [key (min-rank g2 l), val (get (get g2 key) l)]
       (recur (dissoc g2 key) (conj list [key val])))
      (loop [list list, prev-val (second (first list)), cpt 0, offset 0, res {}]
        (if (seq list)
          (if (= (second (first list)) prev-val)
            (recur (rest list)
                   (second (first list))
                   cpt
                   (inc offset)
                   (assoc res
                          (ffirst list)
                          (assoc (get g (ffirst list)) :rank cpt)))
            (recur (rest list)
                   (second (first list))
                   (+ cpt offset)
                   1
                   (assoc res
                          (ffirst list)
                          (assoc (get g (ffirst list)) :rank (+ cpt offset)))))
          res)))))

(defn generate-colors [n]
    (let [step 10]
    (loop [colors {}, current [255.0 160.0 122.0], c 0]
      (if (= c (inc n))
        colors
        (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
               (map #(mod (+ step %) 255) current) (inc c))
        ))))

(defn max-rank
  "Returns the highest rank of the graph"
  [g]
  (if (empty? g)
    nil
    (loop [g g, maxrank (get (second (first g)) :rank)]
      (if (seq g)
        (if (> (get (second (first g)) :rank) maxrank)
          (recur (rest g) (get (second (first g)) :rank))
          (recur (rest g) maxrank))
        maxrank))))

(defn nodes-to-string
  "Returns a string representation of a graph's nodes"
  [g]
  (let [colors (generate-colors (max-rank g))]
    (loop [g (into (sorted-map-by <) g), res ""]
      (if (seq g)
        (let [node (ffirst g),
              red (nth (get colors (get (second (first g)) :rank)) 0),
              green (nth (get colors (get (second (first g)) :rank)) 1),
              blue (nth (get colors (get (second (first g)) :rank)) 2)]
          (recur (rest g) (str res node " [style=filled color=\"" red " " green " " blue "\"]\n")))
        res))))

(defn edges-to-string
  "Returns a string representation of a graph's edges"
  [g]
  (loop [g g, set #{}]
    (if (seq g)
      (let [node (ffirst g)]
        (recur (rest g) (loop [neigh (get (second (first g)) :neigh), set set]
                 (if (seq neigh)
                   (recur (rest neigh) (conj set [(min node (first neigh)), (max node (first neigh))]))
                   set))))
      (loop [set set, res ""]
        (if (seq set)
          (let [fst (ffirst set), snd (second (first set))]
            (recur (rest set) (str res fst " -- " snd "\n")))
          res)))))

(max-rank (rank-nodes (centr/closeness-all {1 {:neigh #{0 4 3}}
                                  0 {:neigh #{1 3}}
                                  3 {:neigh #{0 1 2}}
                                  4 {:neigh #{1}}
                                  2 {:neigh #{3}}}) :close))

(defn to-dot
  "Returns a string in dot format for graph g, each node is colored in relation to its ranking"
  [g]
  (str "graph g{\n" (nodes-to-string g) (edges-to-string g) "}"))
