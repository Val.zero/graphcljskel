(ns graphclj.core
  (:require [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central])
  (:gen-class))

(defn -main
  [& args]
  (let [l (count args)]
    (cond
      (= l 1) (let [seq (tools/readfile (first args))
                    g (tools/rank-nodes (central/closeness-all (graph/gen-graph seq)) :close)
                    output (tools/to-dot g)]
                (print output))
      (= l 2) (let [seq (tools/readfile (first args))
                    g (tools/rank-nodes (central/closeness-all (graph/gen-graph seq)) :close)
                    output (tools/to-dot g)]
                (tools/writefile (second args) output)
                (println "Résultat écrit dans : " (second args)))
      :else (do
              (println "Argument requis : chemin vers un fichier d'entrée.")
              (println "Argument optionel : chemin vers un fichier de sortie.")))))
