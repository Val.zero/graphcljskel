(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))



(defn degrees 
  "Calculates the degree centrality for each node"
  [g]
  (loop [g g, res {}]
    (if (seq g)
      (let [key (ffirst g)
            val (second (first g))]
        (recur (rest g) (assoc res key (assoc val :degree (count (get val :neigh))))))
      res)))

(defn min-map
  "Calculates the key corresponding to the map's minimal value, or nil if empty"
  [map]
  (if (empty? map)
    nil
    (loop [m map, minkey (ffirst m), minval (second (first m))]
      (if (seq m)
        (if (< (second (first m)) minval)
          (recur (rest m) (ffirst m) (second (first m)))
          (recur (rest m) minkey minval))
        minkey))))

(defn new-open
  "Returns the list of opened nodes after visitind the current node, based on its distance, its neighbours, and previously-visited nodes"
  [prev-open node dist neigh cur-res]
  (loop [neigh neigh, open (dissoc prev-open node)]
    ;; On examine les voisins du noeud courant
    (if (seq neigh)
      (if (= (get cur-res (first neigh) ::not-found) ::not-found)
        ;; Si le voisin n'est pas déjà dans la map résultat, on l'ajoute aux noeuds ouverts
        (if (= (get open (first neigh) ::not-found) ::not-found)
          ;; Si le voisin n'est pas encore ouvert, on l'ajoute aux noeuds ouverts avec la distance du noeud courant plus 1
          (recur (rest neigh) (assoc open (first neigh) (inc dist)))
          ;; Si le voisin est déjà ouvert, on met à jour sa distance si besoin
          (recur (rest neigh) (assoc open (first neigh) (min (get open (first neigh)) (inc dist)))))
        ;; Si le voisin est déjà dans la map résultat, pas besoin de le ré-ouvrir
        (recur (rest neigh) open))
      open)))

(defn distance 
  "Calculate the distances of one node to all the others with Dijkstra's algorithm"
  [g n]
  (loop [open {n 0.0}, res {}]
    (if (seq open)
      (let [node (min-map open), dist (get open node)]
        (recur (new-open open node dist (get (get g node) :neigh) res) (assoc res node dist)))
      res)))

(defn inverse-or-zero
  "Returns the number's mathematical inverse, or 0.0"
  [n]
  (if (zero? n)
    0.0
    (float (/ 1 n))))

(defn closeness
  "Returns the closeness for node n in graph g"
  [g n]
  (reduce #(+ (inverse-or-zero (second %2)) %1) 0.0 (distance g n)))

(defn closeness-all
  "Returns the closeness for all nodes in graph g"
  [g]
  (loop [g2 g, res {}]
    (if (seq g2)
      (let [key (ffirst g2)
            val (second (first g2))]
        (recur (rest g2) (assoc res key (assoc val :close (closeness g key)))))
      res)))
