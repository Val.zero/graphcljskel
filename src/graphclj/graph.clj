(ns graphclj.graph
  (:require [clojure.string :as str]))

;; Generate a graph from the lines
(defn gen-graph
  "Returns a hashmap contating the graph"
  [lines]
  (loop [lines lines, res {}]
    (if (seq lines)
      (let [couple (str/split (first lines) #" "),
            fst (Integer/parseInt (first couple)),
            snd (Integer/parseInt (second couple)),
            val1 (get res fst ::not-found),
            val2 (get res snd ::not-found)]
        (recur (rest lines) (-> res
                                (assoc fst (if (= val1 ::not-found)
                                             {:neigh #{snd}}
                                             (assoc val1 :neigh (conj (get val1 :neigh) snd))))
                                (assoc snd (if (= val2 ::not-found)
                                             {:neigh #{fst}}
                                             (assoc val2 :neigh (conj (get val2 :neigh) fst)))))))
      res)))

(defn erdos-renyi-rnd
  "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"
  [n,p]
  ;; On commence par construire la liste des noeuds du graphe
  (loop [i 0, nbr []]
    (if (< i n)
      (recur (inc i) (conj nbr i))
      ;; On parcours la liste des noeuds
      (loop [nodes nbr, lines []]
        (if (seq nodes)
          ;; On parcours la liste des voisins potentiels
          (recur (rest nodes) (loop [neigh (rest nodes), lines lines]
                                (if (seq neigh)
                                  (recur (rest neigh) (if (< (rand) p)
                                                        (conj lines (str (first nodes) " " (first neigh)))
                                                        lines))
                                  lines)))
          (gen-graph lines))))))
