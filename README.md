# graphclj

A Clojure library designed to represent graphs.

## Usage

Use lein run with a csv graph file path to get a DOT format representation of the graph on a terminal.
You can specify another file path to save the output in this file. 
